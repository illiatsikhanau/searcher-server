import express, {Request, Response} from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import records from './records.json';

dotenv.config();
const port = process.env.PORT || 3000;

const app = express();
app.use(cors())
app.get('/api/data', (req: Request, res: Response) => {
    const result = records.filter(record =>
        record.email === req.query.email &&
        (record.number === req.query.number || !req.query.number)
    );
    setTimeout(() => res.send(result), 5000);
});
app.get('*', express.static(__dirname + '/public'));
app.get('*', (req, res) => res.sendFile(__dirname + '/public/index.html'));

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
